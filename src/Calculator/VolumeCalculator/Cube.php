<?php

namespace Pondit\Calculator\VolumeCalculator;


class Cube
{

    protected $value;

    public function __construct($value)
    {
        $this->value = $value;

    }

    public function getCube(){

        $result = $this->value * $this->value * $this->value;
        return $result;
    }

}