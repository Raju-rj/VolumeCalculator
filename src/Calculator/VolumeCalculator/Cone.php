<?php

namespace Pondit\Calculator\VolumeCalculator;


class Cone
{

    protected $radius;
    protected $height;
    protected $pi;

    public function __construct($radius, $height, $pi)
    {
        $this->radius = $radius;
        $this->height = $height;
        $this->pi = $pi;

    }

    public function getCone(){

        $result = $this->pi * $this->radius * $this->radius * $this->height / 3;
        return $result;

    }

}