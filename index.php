<?php

require_once "vendor/autoload.php";

use Pondit\Calculator\VolumeCalculator\Cylinder;
use Pondit\Calculator\VolumeCalculator\Cube;
use Pondit\Calculator\VolumeCalculator\Cone;

$cylinder = new Cylinder(10,20,3.1416);
$cube = new Cube(10);
$cone = new Cone(10, 20, 3.1416);

echo "<h1>Volume Calculator</h1>";

echo "Radiu = 10, Hegiht =  20" ."<br> Cylinder = " . $cylinder->cyl();
echo "<hr>";

echo "Value = 10 <br> Cube = ".$cube->getCube();
echo "<hr>";
echo "Radiu = 10, Hegiht =  20 <br> Cone = ". $cone->getCone();